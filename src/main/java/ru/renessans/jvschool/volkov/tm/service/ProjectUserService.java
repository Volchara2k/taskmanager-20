package ru.renessans.jvschool.volkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.IOwnerUserRepository;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerUserService;
import ru.renessans.jvschool.volkov.tm.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptyDescriptionException;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptyIdException;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptyProjectException;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptyTitleException;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.EmptyUserException;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.EmptyUserIdException;
import ru.renessans.jvschool.volkov.tm.exception.illegal.IllegalIndexException;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public final class ProjectUserService extends AbstractService<Project> implements IOwnerUserService<Project> {

    @NotNull
    private final IOwnerUserRepository<Project> projectRepository;

    public ProjectUserService(@NotNull final IOwnerUserRepository<Project> repository) {
        super(repository);
        this.projectRepository = repository;
    }

    @NotNull
    @Override
    public Project add(
            @Nullable final String userId,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        @NotNull final Project project = new Project(title, description);
        project.setUserId(userId);
        return super.addData(project);
    }

    @Nullable
    @Override
    public Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        @Nullable final Project project = getByIndex(userId, index);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        project.setTitle(title);
        project.setDescription(description);

        return super.updateData(project);
    }

    @Nullable
    @Override
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        @Nullable final Project project = getById(userId, id);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        project.setTitle(title);
        project.setDescription(description);

        return super.updateData(project);
    }

    @Nullable
    @Override
    public Project deleteByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        return this.projectRepository.removeByIndex(userId, index);
    }

    @Nullable
    @Override
    public Project deleteById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(id)) throw new EmptyIdException();
        return this.projectRepository.removeById(userId, id);
    }

    @Nullable
    @Override
    public Project deleteByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(title)) throw new EmptyTitleException();
        return this.projectRepository.removeByTitle(userId, title);
    }

    @NotNull
    @Override
    public Collection<Project> deleteAll(final String userId) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        return this.projectRepository.removeAll(userId);
    }

    @Nullable
    @Override
    public Project getByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        return this.projectRepository.getByIndex(userId, index);
    }

    @Nullable
    @Override
    public Project getById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(id)) throw new EmptyIdException();
        return this.projectRepository.getById(userId, id);
    }

    @Nullable
    @Override
    public Project getByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(title)) throw new EmptyTitleException();
        return this.projectRepository.getByTitle(userId, title);
    }

    @NotNull
    @Override
    public Collection<Project> getAll(@Nullable final String userId) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        return this.projectRepository.getAll(userId);
    }

    @NotNull
    @Override
    public Collection<Project> initDemoData(@Nullable final Collection<User> users) {
        if (Objects.isNull(users)) throw new EmptyUserException();

        @NotNull final List<Project> assignedData = new ArrayList<>();
        users.forEach(user -> {
            @NotNull final Project project =
                    add(user.getId(), DemoDataConst.PROJECT_TITLE, DemoDataConst.PROJECT_DESCRIPTION);
            assignedData.add(project);
        });

        return assignedData;
    }

}