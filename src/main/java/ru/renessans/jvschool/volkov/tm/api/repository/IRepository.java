package ru.renessans.jvschool.volkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.model.AbstractSerializableModel;

import java.util.Collection;

public interface IRepository<T extends AbstractSerializableModel> {

    @NotNull
    Collection<T> setAllData(@NotNull Collection<T> values);

    @NotNull
    T addData(@NotNull T value);

    @NotNull
    T addDataByKey(@NotNull String key, @NotNull T value);

    @Nullable
    T updateData(@NotNull T value);

    @NotNull
    Collection<T> getAllData();

    @Nullable
    T getDataByKey(@NotNull String key);

    @NotNull
    Collection<T> removeAllData();

    @Nullable
    T removeDataByKey(@NotNull String key);

}