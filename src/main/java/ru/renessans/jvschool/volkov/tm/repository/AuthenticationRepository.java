package ru.renessans.jvschool.volkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.IAuthenticationRepository;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.Objects;

public final class AuthenticationRepository extends AbstractRepository<User> implements IAuthenticationRepository<User> {

    @Nullable
    @Override
    public String getUserId(@NotNull final String userKey) {
        @Nullable final User user = super.getDataByKey(userKey);
        if (Objects.isNull(user)) return null;
        @Nullable final String userId = user.getId();
        if (ValidRuleUtil.isNullOrEmpty(userId)) return null;
        return userId;
    }

    @NotNull
    @Override
    public User subscribe(
            @NotNull final String userKey,
            @NotNull final User user
    ) {
        return super.addDataByKey(userKey, user);
    }

    @Override
    public boolean unsubscribe(@NotNull final String userKey) {
        super.removeDataByKey(userKey);
        return true;
    }

}