package ru.renessans.jvschool.volkov.tm.command.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.command.data.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.util.FileUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class DataJsonClearCommand extends AbstractDataCommand {

    @NotNull
    private static final String CMD_JSON_CLEAR = "data-json-clear";

    @NotNull
    private static final String DESC_JSON_CLEAR = "очистить json данные";

    @NotNull
    private static final String NOTIFY_JSON_CLEAR = "Происходит процесс очищения json данных...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_JSON_CLEAR;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_JSON_CLEAR;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.print(NOTIFY_JSON_CLEAR);
        final boolean removeState = FileUtil.deleteFile(JSON_FILE_LOCATE);
        ViewUtil.print(removeState);
    }

}