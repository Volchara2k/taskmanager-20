package ru.renessans.jvschool.volkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.EmptyUserException;
import ru.renessans.jvschool.volkov.tm.model.User;

import java.util.*;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository<User> {

    @Nullable
    @Override
    public User getById(@NotNull final String id) {
        @NotNull final Collection<User> allData = super.getAllData();
        return allData
                .stream()
                .filter(user -> id.equals(user.getId()))
                .findAny()
                .orElse(null);
    }

    @Nullable
    @Override
    public User getByLogin(@NotNull final String login) {
        @NotNull final Collection<User> allData = super.getAllData();
        return allData
                .stream()
                .filter(user -> login.equals(user.getLogin()))
                .findAny()
                .orElse(null);
    }

    @Nullable
    @Override
    public User removeById(@NotNull final String id) {
        @Nullable final User user = getById(id);
        if (Objects.isNull(user)) throw new EmptyUserException();
        return super.removeDataByKey(user.getId());
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        @Nullable final User user = getByLogin(login);
        if (Objects.isNull(user)) throw new EmptyUserException();
        return super.removeDataByKey(user.getId());
    }

}