package ru.renessans.jvschool.volkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICommandService;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.exception.security.NotifyAddFailureException;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Collection;

public final class CommandListCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_COMMANDS = "commands";

    @NotNull
    private static final String ARG_COMMANDS = "-cmd";

    @NotNull
    private static final String DESC_COMMANDS = "вывод списка поддерживаемых терминальных команд";

    @NotNull
    private static final String NOTIFY_COMMANDS = "Список поддерживаемых терминальных команд: \n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_COMMANDS;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARG_COMMANDS;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_COMMANDS;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_COMMANDS);
        @NotNull final ICommandService commandService = super.serviceLocator.getCommandService();
        @Nullable final Collection<AbstractCommand> commands = commandService.getAllTerminalCommands();
        if (ValidRuleUtil.isNullOrEmpty(commands)) throw new NotifyAddFailureException("Терминальные команды");
        ViewUtil.print(commands);
    }

}