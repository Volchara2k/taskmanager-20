package ru.renessans.jvschool.volkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.IOwnerUserRepository;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptyProjectException;
import ru.renessans.jvschool.volkov.tm.model.Project;

import java.util.*;
import java.util.stream.Collectors;

public final class ProjectUserRepository extends AbstractRepository<Project> implements IOwnerUserRepository<Project> {

    @Nullable
    @Override
    public Project removeByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        @Nullable final Project project = getByIndex(userId, index);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        return super.removeDataByKey(project.getId());
    }

    @Nullable
    @Override
    public Project removeById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        @Nullable final Project project = getById(userId, id);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        return super.removeDataByKey(project.getId());
    }

    @Nullable
    @Override
    public Project removeByTitle(
            @NotNull final String userId,
            @NotNull final String title
    ) {
        @Nullable final Project project = getByTitle(userId, title);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        return super.removeDataByKey(project.getId());
    }

    @NotNull
    @Override
    public Collection<Project> removeAll(@NotNull final String userId) {
        @Nullable final Collection<Project> removedProjects = getAll(userId);
        super.dataMap.entrySet().removeIf(taskEntry ->
                userId.equals(taskEntry.getValue().getUserId()));
        return removedProjects;
    }

    @NotNull
    @Override
    public Collection<Project> getAll(@NotNull final String userId) {
        @NotNull final Collection<Project> allData = super.getAllData();
        return allData
                .stream()
                .filter(project -> userId.equals(project.getUserId()))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public Project getByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        @NotNull final List<Project> userProjects = new ArrayList<>(getAll(userId));
        return userProjects.get(index);
    }

    @Nullable
    @Override
    public Project getById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        return getAll(userId)
                .stream()
                .filter(task -> id.equals(task.getId()))
                .findAny()
                .orElse(null);
    }

    @Nullable
    @Override
    public Project getByTitle(
            @NotNull final String userId,
            @NotNull final String title
    ) {
        return getAll(userId)
                .stream()
                .filter(task -> title.equals(task.getTitle()))
                .findAny()
                .orElse(null);
    }

}