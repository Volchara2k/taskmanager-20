package ru.renessans.jvschool.volkov.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.command.data.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.util.FileUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class DataXmlClearCommand extends AbstractDataCommand {

    @NotNull
    private static final String CMD_XML_CLEAR = "data-xml-clear";

    @NotNull
    private static final String DESC_XML_CLEAR = "очистить xml данные";

    @NotNull
    private static final String NOTIFY_XML_CLEAR = "Происходит процесс очищения xml данных...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_XML_CLEAR;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_XML_CLEAR;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.print(NOTIFY_XML_CLEAR);
        final boolean removeState = FileUtil.deleteFile(XML_FILE_LOCATE);
        ViewUtil.print(removeState);
    }

}