package ru.renessans.jvschool.volkov.tm.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.model.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
public final class Domain implements Serializable {

    @NotNull
    private Collection<Project> projects = new ArrayList<>();

    @NotNull
    private Collection<Task> tasks = new ArrayList<>();

    @NotNull
    private Collection<User> users = new ArrayList<>();

}