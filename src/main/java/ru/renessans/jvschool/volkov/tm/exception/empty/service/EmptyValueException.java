package ru.renessans.jvschool.volkov.tm.exception.empty.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyValueException extends AbstractRuntimeException {

    @NotNull
    private static final String EMPTY_VALUE = "Ошибка! Параметр \"значение\" является пустым или null!\n";

    public EmptyValueException() {
        super(EMPTY_VALUE);
    }

}