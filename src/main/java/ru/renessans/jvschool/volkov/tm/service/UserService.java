package ru.renessans.jvschool.volkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.*;
import ru.renessans.jvschool.volkov.tm.exception.security.AccessFailureException;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.PasswordHashUtil;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.*;

public final class UserService extends AbstractService<User> implements IUserService<User> {

    @NotNull
    private final IUserRepository<User> userRepository;

    public UserService(@NotNull final IUserRepository<User> repository) {
        super(repository);
        this.userRepository = repository;
    }

    @Nullable
    @Override
    public User getUserByLogin(@Nullable final String login) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        return this.userRepository.getByLogin(login);
    }

    @NotNull
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();

        @NotNull final String passwordHash = PasswordHashUtil.saltHashLine(password);
        @NotNull final User user = new User(login, passwordHash);
        return addData(user);
    }

    @NotNull
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String firstName
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new EmptyEmailException();

        @NotNull final String passwordHash = PasswordHashUtil.saltHashLine(password);
        @NotNull final User user = new User(login, passwordHash, firstName);
        return addData(user);
    }

    @NotNull
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final UserRole role
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();
        if (Objects.isNull(role)) throw new EmptyUserRoleException();

        @NotNull final String passwordHash = PasswordHashUtil.saltHashLine(password);
        @NotNull final User user = new User(login, passwordHash, role);
        return addData(user);
    }

    @Nullable
    @Override
    public User updatePasswordById(
            @Nullable final String id,
            @Nullable final String newPassword
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(newPassword)) throw new EmptyPasswordException();

        @NotNull final String passwordHash = PasswordHashUtil.saltHashLine(newPassword);
        @Nullable final User user = this.userRepository.getById(id);
        if (Objects.isNull(user)) throw new EmptyUserException();

        user.setPasswordHash(passwordHash);
        return super.updateData(user);
    }

    @Nullable
    @Override
    public User editProfileById(
            @Nullable final String id,
            @Nullable final String firstName
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new EmptyFirstNameException();

        @Nullable final User user = this.userRepository.getById(id);
        if (Objects.isNull(user)) throw new EmptyUserException();

        user.setFirstName(firstName);
        return super.updateData(user);
    }

    @Nullable
    @Override
    public User editProfileById(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new EmptyFirstNameException();
        if (ValidRuleUtil.isNullOrEmpty(lastName)) throw new EmptyLastNameException();

        @Nullable final User user = this.userRepository.getById(id);
        if (Objects.isNull(user)) throw new EmptyUserException();

        user.setFirstName(firstName);
        user.setLastName(lastName);
        return super.updateData(user);
    }

    @Nullable
    @Override
    public User lockUserByLogin(@Nullable final String login) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();

        @Nullable final User user = getUserByLogin(login);
        if (Objects.isNull(user)) throw new EmptyUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException("Операция недоступна!");

        user.setLockdown(true);
        return super.updateData(user);
    }

    @Nullable
    @Override
    public User unlockUserByLogin(@Nullable final String login) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();

        @Nullable final User user = getUserByLogin(login);
        if (Objects.isNull(user)) throw new EmptyUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException("Операция недоступна!");

        user.setLockdown(false);
        return super.updateData(user);
    }

    @Nullable
    @Override
    public User deleteUserById(@Nullable final String id) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyUserIdException();

        @Nullable final User user = getDataByKey(id);
        if (Objects.isNull(user)) throw new EmptyUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException("Операция недоступна!");

        return super.removeDataByKey(id);
    }

    @Nullable
    @Override
    public User deleteUserByLogin(@Nullable final String login) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();

        @Nullable final User user = getUserByLogin(login);
        if (Objects.isNull(user)) throw new EmptyUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException("Операция недоступна!");

        return this.userRepository.removeByLogin(login);
    }

    @NotNull
    @Override
    public Collection<User> initDemoData() {
        return Arrays.asList(
                addUser(DemoDataConst.USER_TEST_LOGIN, DemoDataConst.USER_TEST_PASSWORD),
                addUser(DemoDataConst.USER_DEFAULT_LOGIN, DemoDataConst.USER_DEFAULT_PASSWORD,
                        DemoDataConst.USER_DEFAULT_FIRSTNAME),
                addUser(DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN)
        );
    }

}