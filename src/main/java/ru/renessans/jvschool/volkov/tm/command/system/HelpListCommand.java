package ru.renessans.jvschool.volkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICommandService;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.exception.security.NotifyAddFailureException;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Collection;

public final class HelpListCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_HELP = "help";

    @NotNull
    private static final String ARG_HELP = "-h";

    @NotNull
    private static final String DESC_HELP = "вывод списка команд";

    @NotNull
    private static final String NOTIFY_HELP = "Список команд";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_HELP;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARG_HELP;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_HELP;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_HELP);
        @NotNull final ICommandService commandService = super.serviceLocator.getCommandService();
        @Nullable final Collection<AbstractCommand> commands = commandService.getAllCommands();
        if (ValidRuleUtil.isNullOrEmpty(commands)) throw new NotifyAddFailureException("Описание команд");
        ViewUtil.print(commands);
    }

}