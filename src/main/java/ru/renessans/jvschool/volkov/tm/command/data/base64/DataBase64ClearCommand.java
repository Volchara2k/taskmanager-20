package ru.renessans.jvschool.volkov.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.command.data.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.util.FileUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class DataBase64ClearCommand extends AbstractDataCommand {

    @NotNull
    private static final String CMD_BASE64_CLEAR = "data-base64-clear";

    @NotNull
    private static final String DESC_BASE64_CLEAR = "очистить base64 данные";

    @NotNull
    private static final String NOTIFY_BASE64_CLEAR = "Происходит процесс очищения base64 данных...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_BASE64_CLEAR;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_BASE64_CLEAR;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.print(NOTIFY_BASE64_CLEAR);
        final boolean removeState = FileUtil.deleteFile(BASE64_FILE_LOCATE);
        ViewUtil.print(removeState);
    }

}