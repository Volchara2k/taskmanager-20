package ru.renessans.jvschool.volkov.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerUserService;
import ru.renessans.jvschool.volkov.tm.api.service.IDomainService;
import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.dto.Domain;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptyDomainException;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.model.User;

import java.util.Objects;

@AllArgsConstructor
public final class DomainService implements IDomainService {

    @NotNull
    private final IUserService<User> userService;

    @NotNull
    private final IOwnerUserService<Task> taskService;

    @NotNull
    private final IOwnerUserService<Project> projectService;

    @Override
    public void dataImport(@Nullable final Domain domain) {
        if (Objects.isNull(domain)) throw new EmptyDomainException();
        this.userService.setAllData(domain.getUsers());
        this.taskService.setAllData(domain.getTasks());
        this.projectService.setAllData(domain.getProjects());
    }

    @Override
    public void dataExport(@Nullable final Domain domain) {
        if (Objects.isNull(domain)) throw new EmptyDomainException();
        domain.setUsers(this.userService.getAllData());
        domain.setTasks(this.taskService.getAllData());
        domain.setProjects(this.projectService.getAllData());
    }

}