package ru.renessans.jvschool.volkov.tm.exception.empty.user;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyUserException extends AbstractRuntimeException {

    @NotNull
    private static final String EMPTY_USER =
            "Ошибка! Параметр \"пользователь\" является null!\n";

    public EmptyUserException() {
        super(EMPTY_USER);
    }

}