package ru.renessans.jvschool.volkov.tm.exception.empty.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyKeyException extends AbstractRuntimeException {

    @NotNull
    private static final String EMPTY_KEY = "Ошибка! Параметр \"ключ значения\" является пустым или null!\n";

    public EmptyKeyException() {
        super(EMPTY_KEY);
    }

}