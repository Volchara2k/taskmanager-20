package ru.renessans.jvschool.volkov.tm.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptyFileException;

import java.io.File;

@UtilityClass
public class DataMarshalizerUtil {

    @SneakyThrows
    public <T> void writeToJson(
            @NotNull final T t,
            @NotNull final String filename
    ) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final File file = FileUtil.createEmptyFile(filename);
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, t);
    }

    @Nullable
    @SneakyThrows
    public <T> T readFromJson(
            @NotNull final String filename,
            @NotNull final Class<T> tClass
    ) {
        if (FileUtil.fileIsNotExists(filename)) throw new EmptyFileException();
        @NotNull final File file = new File(filename);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(file, tClass);
    }

    @SneakyThrows
    public <T> void writeToXml(
            @NotNull final T t,
            @NotNull final String filename
    ) {
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final File file = FileUtil.createEmptyFile(filename);
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, t);
    }

    @Nullable
    @SneakyThrows
    public <T> T readFromXml(
            @NotNull final String filename,
            @NotNull final Class<T> tClass
    ) {
        if (FileUtil.fileIsNotExists(filename)) throw new EmptyFileException();
        @NotNull final File file = new File(filename);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(file, tClass);
    }

    @SneakyThrows
    public <T> void writeToYaml(
            @NotNull final T t,
            @NotNull final String filename
    ) {
        @NotNull final YAMLFactory yamlFactory = new YAMLFactory();
        @NotNull final ObjectMapper objectMapper = new YAMLMapper(yamlFactory);
        @NotNull final File file = FileUtil.createEmptyFile(filename);
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, t);
    }

    @Nullable
    @SneakyThrows
    public <T> T readFromYaml(
            @NotNull final String filename,
            @NotNull final Class<T> tClass
    ) {
        if (FileUtil.fileIsNotExists(filename)) throw new EmptyFileException();
        @NotNull final File file = new File(filename);
        @NotNull final YAMLFactory yamlFactory = new YAMLFactory();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper(yamlFactory);
        return objectMapper.readValue(file, tClass);
    }

}