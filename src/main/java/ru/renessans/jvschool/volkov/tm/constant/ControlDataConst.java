package ru.renessans.jvschool.volkov.tm.constant;

import org.jetbrains.annotations.NotNull;

public interface ControlDataConst {

    @NotNull
    String EXIT_FACTOR = "exit";

}