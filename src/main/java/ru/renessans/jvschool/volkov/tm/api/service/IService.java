package ru.renessans.jvschool.volkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.model.AbstractSerializableModel;

import java.util.Collection;

public interface IService<T extends AbstractSerializableModel> {

    @NotNull
    Collection<T> setAllData(@Nullable Collection<T> values);

    @NotNull
    T addData(@Nullable T value);

    @NotNull
    Collection<T> getAllData();

    @Nullable
    T updateData(@Nullable T value);

    @Nullable
    T getDataByKey(@Nullable String key);

    @NotNull
    Collection<T> removeAllData();

    @Nullable
    T removeDataByKey(@Nullable String key);

}