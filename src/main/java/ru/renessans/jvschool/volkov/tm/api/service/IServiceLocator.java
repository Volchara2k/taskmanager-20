package ru.renessans.jvschool.volkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.model.User;

public interface IServiceLocator {

    @NotNull
    IUserService<User> getUserService();

    @NotNull
    IAuthenticationService<User> getAuthenticationService();

    @NotNull
    IOwnerUserService<Task> getTaskService();

    @NotNull
    IOwnerUserService<Project> getProjectService();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IDomainService getDomainService();

}