package ru.renessans.jvschool.volkov.tm.command.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IDomainService;
import ru.renessans.jvschool.volkov.tm.command.data.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.dto.Domain;
import ru.renessans.jvschool.volkov.tm.util.DataMarshalizerUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class DataJsonExportCommand extends AbstractDataCommand {

    @NotNull
    private static final String CMD_JSON_EXPORT = "data-json-export";

    @NotNull
    private static final String DESC_JSON_EXPORT = "экспортировать домен в json вид";

    @NotNull
    private static final String NOTIFY_JSON_EXPORT = "Происходит процесс выгрузки домена в json вид...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_JSON_EXPORT;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_JSON_EXPORT;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.print(NOTIFY_JSON_EXPORT);
        @NotNull final Domain domain = new Domain();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.dataExport(domain);
        DataMarshalizerUtil.writeToJson(domain, JSON_FILE_LOCATE);

        ViewUtil.print(domain.getUsers());
        ViewUtil.print(domain.getTasks());
        ViewUtil.print(domain.getProjects());
    }

}