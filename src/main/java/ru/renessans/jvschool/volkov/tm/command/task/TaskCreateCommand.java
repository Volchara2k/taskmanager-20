package ru.renessans.jvschool.volkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerUserService;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    private static final String CMD_TASK_CREATE = "task-create";

    @NotNull
    private static final String DESC_TASK_CREATE = "добавить новую задачу";

    @NotNull
    private static final String NOTIFY_TASK_CREATE =
            "Для создания задачи введите её заголовок или заголовок с описанием.";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_TASK_CREATE;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_TASK_CREATE;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_TASK_CREATE);
        @NotNull final String title = ViewUtil.getLine();
        @NotNull final String description = ViewUtil.getLine();

        @NotNull final IAuthenticationService<User> authService = super.serviceLocator.getAuthenticationService();
        @Nullable final String userId = authService.getUserId();
        @NotNull final IOwnerUserService<Task> taskService = super.serviceLocator.getTaskService();

        @Nullable final Task task = taskService.add(userId, title, description);
        ViewUtil.print(task);
    }

}