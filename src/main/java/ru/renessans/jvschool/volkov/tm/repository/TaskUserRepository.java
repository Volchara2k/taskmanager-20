package ru.renessans.jvschool.volkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.IOwnerUserRepository;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptyTaskException;
import ru.renessans.jvschool.volkov.tm.model.Task;

import java.util.*;
import java.util.stream.Collectors;

public final class TaskUserRepository extends AbstractRepository<Task> implements IOwnerUserRepository<Task> {

    @Nullable
    @Override
    public Task removeByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        @Nullable final Task task = getByIndex(userId, index);
        if (Objects.isNull(task)) throw new EmptyTaskException();
        return super.removeDataByKey(task.getId());
    }

    @Nullable
    @Override
    public Task removeById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        @Nullable final Task task = getById(userId, id);
        if (Objects.isNull(task)) throw new EmptyTaskException();
        return super.removeDataByKey(task.getId());
    }

    @Nullable
    @Override
    public Task removeByTitle(
            @NotNull final String userId,
            @NotNull final String title
    ) {
        @Nullable final Task task = getByTitle(userId, title);
        if (Objects.isNull(task)) throw new EmptyTaskException();
        return super.removeDataByKey(task.getId());
    }

    @NotNull
    @Override
    public Collection<Task> removeAll(@NotNull final String userId) {
        @Nullable final Collection<Task> removedTasks = getAll(userId);
        super.dataMap.entrySet().removeIf(taskEntry ->
                userId.equals(taskEntry.getValue().getUserId()));
        return removedTasks;
    }

    @NotNull
    @Override
    public Collection<Task> getAll(@NotNull final String userId) {
        @NotNull final Collection<Task> allData = super.getAllData();
        return allData
                .stream()
                .filter(task -> userId.equals(task.getUserId()))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public Task getByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        @NotNull final List<Task> userTasks = new ArrayList<>(getAll(userId));
        return userTasks.get(index);
    }

    @Nullable
    @Override
    public Task getById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        return getAll(userId)
                .stream()
                .filter(task -> id.equals(task.getId()))
                .findAny()
                .orElse(null);
    }

    @Nullable
    @Override
    public Task getByTitle(
            @NotNull final String userId,
            @NotNull final String title
    ) {
        return getAll(userId)
                .stream()
                .filter(task -> title.equals(task.getTitle()))
                .findAny()
                .orElse(null);
    }

}