package ru.renessans.jvschool.volkov.tm.command.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IDomainService;
import ru.renessans.jvschool.volkov.tm.command.data.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.dto.Domain;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptyDomainException;
import ru.renessans.jvschool.volkov.tm.util.DataMarshalizerUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Objects;

public final class DataJsonImportCommand extends AbstractDataCommand {

    @NotNull
    private static final String CMD_JSON_IMPORT = "data-json-import";

    @NotNull
    private static final String DESC_JSON_IMPORT = "импортировать домен из json вида";

    @NotNull
    private static final String NOTIFY_JSON_IMPORT = "Происходит процесс загрузки домена из json вида...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_JSON_IMPORT;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_JSON_IMPORT;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.print(NOTIFY_JSON_IMPORT);
        @Nullable final Domain domain;
        domain = DataMarshalizerUtil.readFromJson(JSON_FILE_LOCATE, Domain.class);

        if (Objects.isNull(domain)) throw new EmptyDomainException();
        @NotNull final IDomainService domainService = super.serviceLocator.getDomainService();
        domainService.dataImport(domain);

        ViewUtil.print(domain.getUsers());
        ViewUtil.print(domain.getTasks());
        ViewUtil.print(domain.getProjects());
    }

}