package ru.renessans.jvschool.volkov.tm.exception.illegal;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class IllegalInstantiationException extends AbstractRuntimeException {

    @NotNull
    private static final String HASH_ALGORITHM_ILLEGAL =
            "Ошибка! Параметр \"объект класса команда\" является нелегальным и не может быть создан!\n";

    public IllegalInstantiationException() {
        super(HASH_ALGORITHM_ILLEGAL);
    }

}