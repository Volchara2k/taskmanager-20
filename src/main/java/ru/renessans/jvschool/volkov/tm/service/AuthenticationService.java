package ru.renessans.jvschool.volkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.IAuthenticationRepository;
import ru.renessans.jvschool.volkov.tm.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.enumeration.AuthState;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.EmptyEmailException;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.EmptyLoginException;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.EmptyPasswordException;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.EmptyUserRoleException;
import ru.renessans.jvschool.volkov.tm.exception.security.AccessFailureException;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.PasswordHashUtil;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.Arrays;
import java.util.Objects;
import java.util.UUID;

public final class AuthenticationService extends AbstractService<User> implements IAuthenticationService<User> {

    @NotNull
    private static final String CURRENT_USER_KEY = UUID.randomUUID().toString();

    @NotNull
    private final IAuthenticationRepository<User> authenticationRepository;

    @NotNull
    private final IUserService<User> userService;

    public AuthenticationService(
            @NotNull final IAuthenticationRepository<User> repository,
            @NotNull final IUserService<User> userService
    ) {
        super(repository);
        this.authenticationRepository = repository;
        this.userService = userService;
    }

    @Nullable
    @Override
    public String getUserId() {
        return this.authenticationRepository.getUserId(CURRENT_USER_KEY);
    }

    @NotNull
    @Override
    public UserRole getUserRole() {
        @Nullable final String userId = this.authenticationRepository.getUserId(CURRENT_USER_KEY);
        if (ValidRuleUtil.isNullOrEmpty(userId)) return UserRole.UNKNOWN;

        @Nullable final User user = this.userService.getDataByKey(userId);
        if (Objects.isNull(user)) return UserRole.UNKNOWN;

        return user.getRole();
    }

    @Override
    public void verifyPermissions(@Nullable final UserRole[] userRoles) {
        if (Objects.isNull(userRoles)) return;

        @NotNull final UserRole userRole = getUserRole();
        boolean containsTypes = Arrays.asList(userRoles).contains(userRole);
        if (containsTypes) return;

        throw new AccessFailureException(
                String.format("%s, %s", AuthState.NEED_LOG_IN.getTitle(), AuthState.NO_ACCESS_RIGHTS.getTitle())
        );
    }

    @NotNull
    @Override
    public AuthState signIn(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();

        @Nullable final User user = this.userService.getUserByLogin(login);
        if (Objects.isNull(user)) return AuthState.USER_NOT_FOUND;
        if (user.getLockdown()) return AuthState.LOCKDOWN_PROFILE;

        @NotNull final String passwordHash = PasswordHashUtil.saltHashLine(password);
        if (!passwordHash.equals(user.getPasswordHash())) return AuthState.INVALID_PASSWORD;

        this.authenticationRepository.subscribe(CURRENT_USER_KEY, user);
        return AuthState.SUCCESS;
    }

    @NotNull
    @Override
    public User signUp(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();
        return this.userService.addUser(login, password);
    }

    @NotNull
    @Override
    public User signUp(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();
        if (ValidRuleUtil.isNullOrEmpty(email)) throw new EmptyEmailException();

        return this.userService.addUser(login, password, email);
    }

    @NotNull
    @Override
    public User signUp(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final UserRole role
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();
        if (Objects.isNull(role)) throw new EmptyUserRoleException();

        return this.userService.addUser(login, password, role);
    }

    @Override
    public boolean logOut() {
        return this.authenticationRepository.unsubscribe(CURRENT_USER_KEY);
    }

}