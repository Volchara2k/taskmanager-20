package ru.renessans.jvschool.volkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.model.AbstractSerializableModel;
import ru.renessans.jvschool.volkov.tm.model.User;

import java.util.Collection;

public interface IOwnerUserService<T extends AbstractSerializableModel> extends IService<T> {

    @NotNull
    T add(@Nullable String userId, @Nullable String title, @Nullable String description);

    @Nullable
    T updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String title, @Nullable String description);

    @Nullable
    T updateById(@Nullable String userId, @Nullable String id, @Nullable String title, @Nullable String description);

    @Nullable
    T deleteByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    T deleteById(@Nullable String userId, @Nullable String id);

    @Nullable
    T deleteByTitle(@Nullable String userId, @Nullable String title);

    @NotNull
    Collection<T> deleteAll(@Nullable String userId);

    @Nullable
    T getByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    T getById(@Nullable String userId, @Nullable String id);

    @Nullable
    T getByTitle(@Nullable String userId, @Nullable String title);

    @NotNull
    Collection<T> getAll(@Nullable String userId);

    @NotNull
    Collection<T> initDemoData(@Nullable Collection<User> users);

}