package ru.renessans.jvschool.volkov.tm.exception.security;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class NotifyAddFailureException extends AbstractRuntimeException {

    @NotNull
    private static final String INCORRECT_COMMAND_REGISTRATION =
            "Ошибка! Сбой добавления уведомлений для команд: %s!\n";

    public NotifyAddFailureException(@NotNull final String message) {
        super(String.format(INCORRECT_COMMAND_REGISTRATION, message));
    }

}