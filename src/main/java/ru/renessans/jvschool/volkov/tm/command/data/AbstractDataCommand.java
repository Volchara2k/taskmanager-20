package ru.renessans.jvschool.volkov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected static final String BIN_FILE_LOCATE = "./data.bin";

    @NotNull
    protected static final String BASE64_FILE_LOCATE = "./data.base64";

    @NotNull
    protected static final String JSON_FILE_LOCATE = "./data.json";

    @NotNull
    protected static final String XML_FILE_LOCATE = "./data.xml";

    @NotNull
    protected static final String YAML_FILE_LOCATE = "./data.yaml";

    @NotNull
    @Override
    public UserRole[] permissions() {
        return new UserRole[]{UserRole.ADMIN};
    }

}