package ru.renessans.jvschool.volkov.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.illegal.IllegalHashAlgorithmException;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@UtilityClass
public final class PasswordHashUtil {

    @NotNull
    private static final String SEPARATOR_KEY = "t2ijtnoi23nfog";

    private static final int ITERATOR_KEY = 15423;

    @NotNull
    public String saltHashLine(@NotNull final String line) {
        @NotNull String hashLine = line;
        for (int i = 0; i < ITERATOR_KEY; i++) {
            hashLine = hashLineMD5(SEPARATOR_KEY + line + SEPARATOR_KEY);
        }
        return hashLine;
    }

    @NotNull
    public String hashLineMD5(@NotNull final String line) {
        try {
            @NotNull final MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            final byte[] byteArray = messageDigest.digest(line.getBytes(StandardCharsets.UTF_8));
            @NotNull final StringBuilder stringBuilder = new StringBuilder();

            for (final byte symbol : byteArray) {
                stringBuilder.append(Integer.toHexString((symbol & 0xFF) | 0x100), 1, 3);
            }

            return stringBuilder.toString();
        } catch (final NoSuchAlgorithmException e) {
            throw new IllegalHashAlgorithmException();
        }
    }

}