package ru.renessans.jvschool.volkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerUserService;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Collection;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    private static final String CMD_TASK_LIST = "task-list";

    @NotNull
    private static final String DESC_TASK_LIST = "вывод списка задач";

    @NotNull
    private static final String NOTIFY_TASK_LIST = "Текущий список задач: \n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_TASK_LIST;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_TASK_LIST;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_TASK_LIST);
        @NotNull final IAuthenticationService<User> authService = super.serviceLocator.getAuthenticationService();
        @Nullable final String userId = authService.getUserId();
        @NotNull final IOwnerUserService<Task> taskService = super.serviceLocator.getTaskService();
        @Nullable final Collection<Task> tasks = taskService.getAll(userId);
        ViewUtil.print(tasks);
    }

}