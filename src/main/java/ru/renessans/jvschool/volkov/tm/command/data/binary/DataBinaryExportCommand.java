package ru.renessans.jvschool.volkov.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IDomainService;
import ru.renessans.jvschool.volkov.tm.command.data.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.dto.Domain;
import ru.renessans.jvschool.volkov.tm.util.DataSerializerUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;


public final class DataBinaryExportCommand extends AbstractDataCommand {

    @NotNull
    private static final String CMD_BIN_EXPORT = "data-bin-export";

    @NotNull
    private static final String DESC_BIN_EXPORT = "экспортировать домен в бинарный вид";

    @NotNull
    private static final String NOTIFY_BIN_EXPORT = "Происходит процесс выгрузки домена в бинарный вид...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_BIN_EXPORT;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_BIN_EXPORT;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.print(NOTIFY_BIN_EXPORT);

        @NotNull final Domain domain = new Domain();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.dataExport(domain);

        try (@NotNull final DataSerializerUtil dataInterchange = new DataSerializerUtil()) {
            dataInterchange.writeToBin(domain, BIN_FILE_LOCATE);
        }

        ViewUtil.print(domain.getUsers());
        ViewUtil.print(domain.getTasks());
        ViewUtil.print(domain.getProjects());
    }

}