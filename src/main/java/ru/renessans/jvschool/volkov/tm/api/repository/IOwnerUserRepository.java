package ru.renessans.jvschool.volkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.model.AbstractSerializableModel;

import java.util.Collection;

public interface IOwnerUserRepository<T extends AbstractSerializableModel> extends IRepository<T> {

    @Nullable
    T removeByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    T removeById(@NotNull String userId, @NotNull String id);

    @Nullable
    T removeByTitle(@NotNull String userId, @NotNull String title);

    @NotNull
    Collection<T> removeAll(@NotNull String userId);

    @NotNull
    Collection<T> getAll(@NotNull String userId);

    @Nullable
    T getByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    T getById(@NotNull String userId, @NotNull String id);

    @Nullable
    T getByTitle(@NotNull String userId, @NotNull String title);

}