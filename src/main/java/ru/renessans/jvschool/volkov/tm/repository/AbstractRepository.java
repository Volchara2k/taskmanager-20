package ru.renessans.jvschool.volkov.tm.repository;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.IRepository;
import ru.renessans.jvschool.volkov.tm.model.AbstractSerializableModel;

import java.util.*;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class AbstractRepository<T extends AbstractSerializableModel> implements IRepository<T> {

    @NotNull
    protected final Map<String, T> dataMap = new LinkedHashMap<>();

    @NotNull
    @Override
    public Collection<T> setAllData(@NotNull final Collection<T> values) {
        values.forEach(this::addData);
        return values;
    }

    @NotNull
    @Override
    public T addData(@NotNull final T value) {
        this.dataMap.put(value.getId(), value);
        return value;
    }

    @NotNull
    @Override
    public T addDataByKey(
            @NotNull final String key,
            @NotNull final T value
    ) {
        this.dataMap.put(key, value);
        return value;
    }

    @Nullable
    @Override
    public T updateData(@NotNull final T value) {
        @Nullable final T result = this.dataMap.putIfAbsent(value.getId(), value);
        return result;
    }

    @NotNull
    @Override
    public Collection<T> getAllData() {
        return new ArrayList<>(this.dataMap.values());
    }

    @Nullable
    @Override
    public T getDataByKey(@NotNull final String key) {
        return this.dataMap.get(key);
    }

    @NotNull
    @Override
    public Collection<T> removeAllData() {
        @NotNull final Collection<T> values = getAllData();
        this.dataMap.clear();
        return values;
    }

    @Nullable
    @Override
    public T removeDataByKey(@NotNull final String key) {
        @Nullable final T value = getDataByKey(key);
        if (Objects.isNull(value)) return null;
        this.dataMap.remove(key);
        return value;
    }

}