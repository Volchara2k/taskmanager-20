package ru.renessans.jvschool.volkov.tm.command.data.yaml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IDomainService;
import ru.renessans.jvschool.volkov.tm.command.data.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.dto.Domain;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptyDomainException;
import ru.renessans.jvschool.volkov.tm.util.DataMarshalizerUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Objects;

public final class DataYamlImportCommand extends AbstractDataCommand {

    @NotNull
    private static final String CMD_YAML_IMPORT = "data-yaml-import";

    @NotNull
    private static final String DESC_YAML_IMPORT = "импортировать домен из yaml вида";

    @NotNull
    private static final String NOTIFY_YAML_IMPORT = "Происходит процесс загрузки домена из yaml вида...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_YAML_IMPORT;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_YAML_IMPORT;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.print(NOTIFY_YAML_IMPORT);
        @Nullable final Domain domain;
        domain = DataMarshalizerUtil.readFromYaml(YAML_FILE_LOCATE, Domain.class);

        if (Objects.isNull(domain)) throw new EmptyDomainException();
        @NotNull final IDomainService domainService = super.serviceLocator.getDomainService();
        domainService.dataImport(domain);

        ViewUtil.print(domain.getUsers());
        ViewUtil.print(domain.getTasks());
        ViewUtil.print(domain.getProjects());
    }

}