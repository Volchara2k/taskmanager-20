package ru.renessans.jvschool.volkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerUserService;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class TaskViewByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String CMD_TASK_VIEW_BY_INDEX = "task-view-by-index";

    @NotNull
    private static final String DESC_TASK_VIEW_BY_INDEX = "просмотреть задачу по индексу";

    @NotNull
    private static final String NOTIFY_TASK_VIEW_BY_INDEX = "Для отображения задачи по индексу введите индекс задачи из списка ниже.\n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_TASK_VIEW_BY_INDEX;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_TASK_VIEW_BY_INDEX;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_TASK_VIEW_BY_INDEX);
        @NotNull final Integer index = ViewUtil.getInteger() - 1;

        @NotNull final IAuthenticationService<User> authService = super.serviceLocator.getAuthenticationService();
        @Nullable final String userId = authService.getUserId();
        @NotNull final IOwnerUserService<Task> taskService = super.serviceLocator.getTaskService();

        @Nullable final Task task = taskService.getByIndex(userId, index);
        ViewUtil.print(task);
    }

}