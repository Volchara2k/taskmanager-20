package ru.renessans.jvschool.volkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.model.AbstractSerializableModel;

public interface IAuthenticationRepository<T extends AbstractSerializableModel> extends IRepository<T> {

    @Nullable
    String getUserId(@NotNull String userKey);

    @NotNull
    T subscribe(@NotNull String userKey, @NotNull T user);

    boolean unsubscribe(@NotNull String userKey);

}