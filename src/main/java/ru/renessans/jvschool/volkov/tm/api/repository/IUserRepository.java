package ru.renessans.jvschool.volkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.model.AbstractSerializableModel;
import ru.renessans.jvschool.volkov.tm.model.User;

public interface IUserRepository<T extends AbstractSerializableModel> extends IRepository<T> {

    @Nullable
    User getById(@NotNull String id);

    @Nullable
    User getByLogin(@NotNull String login);

    @Nullable
    User removeById(@NotNull String id);

    @Nullable
    User removeByLogin(@NotNull String login);

}