package ru.renessans.jvschool.volkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.model.AbstractSerializableModel;
import ru.renessans.jvschool.volkov.tm.model.User;

import java.util.Collection;

public interface IUserService<T extends AbstractSerializableModel> extends IService<T> {

    @Nullable
    User getUserByLogin(@Nullable String login);

    @NotNull
    User addUser(@Nullable String login, @Nullable String password);

    @NotNull
    User addUser(@Nullable String login, @Nullable String password, @Nullable String firstName);

    @NotNull
    User addUser(@Nullable String login, @Nullable String password, @Nullable UserRole role);

    @Nullable
    User editProfileById(@Nullable String id, @Nullable String firstName);

    @Nullable
    User editProfileById(@Nullable String id, @Nullable String firstName, @Nullable String lastName);

    @Nullable
    User updatePasswordById(@Nullable String id, @Nullable String newPassword);

    @Nullable
    User lockUserByLogin(@Nullable String login);

    @Nullable
    User unlockUserByLogin(@Nullable String login);

    @Nullable
    User deleteUserById(@Nullable String id);

    @Nullable
    User deleteUserByLogin(@Nullable String login);

    @NotNull
    Collection<User> initDemoData();

}