package ru.renessans.jvschool.volkov.tm.exception.illegal;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class IllegalHashAlgorithmException extends AbstractRuntimeException {

    @NotNull
    private static final String HASH_ALGORITHM_ILLEGAL = "Ошибка! Параметр \"хеш строки\" является нелегальным!\n";

    public IllegalHashAlgorithmException() {
        super(HASH_ALGORITHM_ILLEGAL);
    }

}