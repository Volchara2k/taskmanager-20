package ru.renessans.jvschool.volkov.tm.service;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.IRepository;
import ru.renessans.jvschool.volkov.tm.api.service.IService;
import ru.renessans.jvschool.volkov.tm.exception.empty.service.EmptyKeyException;
import ru.renessans.jvschool.volkov.tm.exception.empty.service.EmptyValueException;
import ru.renessans.jvschool.volkov.tm.exception.empty.service.EmptyValuesException;
import ru.renessans.jvschool.volkov.tm.model.AbstractSerializableModel;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.Collection;
import java.util.Objects;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class AbstractService <T extends AbstractSerializableModel> implements IService<T> {

    @NotNull
    private final IRepository<T> repository;

    @NotNull
    @Override
    public Collection<T> setAllData(@Nullable final Collection<T> values) {
        if (ValidRuleUtil.isNullOrEmpty(values)) throw new EmptyValueException();
        removeAllData();
        return this.repository.setAllData(values);
    }

    @NotNull
    @Override
    public T addData(@Nullable final T value) {
        if (Objects.isNull(value)) throw new EmptyValuesException();
        return this.repository.addData(value);
    }

    @Nullable
    @Override
    public T updateData(@Nullable final T value) {
        if (Objects.isNull(value)) throw new EmptyValueException();
        return this.repository.updateData(value);
    }

    @NotNull
    @Override
    public Collection<T> getAllData() {
        return this.repository.getAllData();
    }

    @Nullable
    @Override
    public T getDataByKey(@Nullable final String key) {
        if (ValidRuleUtil.isNullOrEmpty(key)) throw new EmptyKeyException();
        return this.repository.getDataByKey(key);
    }

    @NotNull
    @Override
    public Collection<T> removeAllData() {
        return repository.removeAllData();
    }

    @Nullable
    @Override
    public T removeDataByKey(@Nullable final String key) {
        if (ValidRuleUtil.isNullOrEmpty(key)) throw new EmptyKeyException();
        return this.repository.removeDataByKey(key);
    }

}