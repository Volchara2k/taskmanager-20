package ru.renessans.jvschool.volkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class UserUnlockCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_USER_UNLOCK = "user-unlock";

    @NotNull
    private static final String DESC_USER_UNLOCK = "разблокировать пользователя (администратор)";

    @NotNull
    private static final String NOTIFY_USER_UNLOCK = "Для разблокирования пользователя в системе введите его логин. \n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_USER_UNLOCK;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_USER_UNLOCK;
    }

    @Nullable
    @Override
    public UserRole[] permissions() {
        return new UserRole[]{UserRole.ADMIN};
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_USER_UNLOCK);
        @NotNull final String login = ViewUtil.getLine();
        @NotNull final IUserService<User> userService = super.serviceLocator.getUserService();
        @Nullable final User user = userService.unlockUserByLogin(login);
        ViewUtil.print(user);
    }

}