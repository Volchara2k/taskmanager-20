package ru.renessans.jvschool.volkov.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.command.data.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.util.FileUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class DataBinaryClearCommand extends AbstractDataCommand {

    @NotNull
    private static final String CMD_BIN_CLEAR = "data-bin-clear";

    @NotNull
    private static final String DESC_BIN_CLEAR = "очистить бинарные данные";

    @NotNull
    private static final String NOTIFY_BIN_CLEAR = "Происходит процесс очищения бинарных данных...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_BIN_CLEAR;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_BIN_CLEAR;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.print(NOTIFY_BIN_CLEAR);
        final boolean removeState = FileUtil.deleteFile(BIN_FILE_LOCATE);
        ViewUtil.print(removeState);
    }

}