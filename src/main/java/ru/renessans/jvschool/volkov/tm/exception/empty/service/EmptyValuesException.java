package ru.renessans.jvschool.volkov.tm.exception.empty.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyValuesException extends AbstractRuntimeException {

    @NotNull
    private static final String EMPTY_VALUES = "Ошибка! Параметр \"значения\" является null!\n";

    public EmptyValuesException() {
        super(EMPTY_VALUES);
    }

}