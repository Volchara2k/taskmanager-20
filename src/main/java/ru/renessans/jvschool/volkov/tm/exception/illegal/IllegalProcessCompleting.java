package ru.renessans.jvschool.volkov.tm.exception.illegal;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class IllegalProcessCompleting extends AbstractRuntimeException {

    @NotNull
    private static final String PROCESS_COMPLETING_ILLEGAL =
            "Ошибка! Нелегальный результат операции!\n";

    public IllegalProcessCompleting() {
        super(PROCESS_COMPLETING_ILLEGAL);
    }

}