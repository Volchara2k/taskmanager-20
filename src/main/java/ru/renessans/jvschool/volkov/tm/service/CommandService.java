package ru.renessans.jvschool.volkov.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.tm.api.service.ICommandService;
import ru.renessans.jvschool.volkov.tm.api.service.IServiceLocator;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.exception.empty.command.EmptyCommandException;
import ru.renessans.jvschool.volkov.tm.exception.empty.command.EmptyServiceLocatorException;

import java.util.*;

@AllArgsConstructor
public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    @Override
    public Collection<AbstractCommand> initCommands(@Nullable final IServiceLocator serviceLocator) {
        if (Objects.isNull(serviceLocator)) throw new EmptyServiceLocatorException();
        @NotNull final Collection<AbstractCommand> commands = this.commandRepository.getAllCommands();
        commands.forEach(command -> command.setServiceLocator(serviceLocator));
        return commands;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAllCommands() {
        return this.commandRepository.getAllCommands();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAllTerminalCommands() {
        return this.commandRepository.getAllTerminalCommands();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAllArgumentCommands() {
        return this.commandRepository.getAllArgumentCommands();
    }

    @Nullable
    @Override
    public AbstractCommand getTerminalCommand(@Nullable final String command) {
        if (Objects.isNull(command)) throw new EmptyCommandException();
        return this.commandRepository.getTerminalCommand(command);
    }

    @Nullable
    @Override
    public AbstractCommand getArgumentCommand(@Nullable final String command) {
        if (Objects.isNull(command)) throw new EmptyCommandException();
        return this.commandRepository.getArgumentCommand(command);
    }

}