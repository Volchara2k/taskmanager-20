package ru.renessans.jvschool.volkov.tm.exception.empty.data;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyTitleException extends AbstractRuntimeException {

    @NotNull
    private static final String EMPTY_TITLE = "Ошибка! Параметр \"заголовок\" является пустым или null!\n";

    public EmptyTitleException() {
        super(EMPTY_TITLE);
    }

}