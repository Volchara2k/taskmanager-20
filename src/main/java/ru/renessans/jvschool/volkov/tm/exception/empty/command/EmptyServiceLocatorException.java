package ru.renessans.jvschool.volkov.tm.exception.empty.command;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyServiceLocatorException extends AbstractRuntimeException {

    @NotNull
    private static final String EMPTY_LOCATOR = "Ошибка! Параметр \"служба поиска\" является null!\n";

    public EmptyServiceLocatorException() {
        super(EMPTY_LOCATOR);
    }

}