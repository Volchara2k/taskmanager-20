package ru.renessans.jvschool.volkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class UserDeleteCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_USER_DELETE = "user-delete";

    @NotNull
    private static final String DESC_USER_DELETE = "удалить пользователя (администратор)";

    @NotNull
    private static final String NOTIFY_USER_DELETE = "Для удаления пользователя в системе введите его логин. \n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_USER_DELETE;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_USER_DELETE;
    }

    @Nullable
    @Override
    public UserRole[] permissions() {
        return new UserRole[]{UserRole.ADMIN};
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_USER_DELETE);
        @NotNull final String login = ViewUtil.getLine();
        @NotNull final IUserService<User> userService = super.serviceLocator.getUserService();
        @Nullable final User user = userService.deleteUserByLogin(login);
        ViewUtil.print(user);
    }

}