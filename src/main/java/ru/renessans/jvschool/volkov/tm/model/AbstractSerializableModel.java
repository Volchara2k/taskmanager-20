package ru.renessans.jvschool.volkov.tm.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@ToString
public abstract class AbstractSerializableModel implements Serializable {

    @NotNull
    protected String id = UUID.randomUUID().toString();

}