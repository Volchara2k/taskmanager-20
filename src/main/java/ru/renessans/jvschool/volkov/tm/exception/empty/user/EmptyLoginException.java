package ru.renessans.jvschool.volkov.tm.exception.empty.user;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyLoginException extends AbstractRuntimeException {

    @NotNull
    private static final String EMPTY_LOGIN = "Ошибка! Параметр \"логин\" является пустым или null!\n";

    public EmptyLoginException() {
        super(EMPTY_LOGIN);
    }

}