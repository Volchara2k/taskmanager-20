package ru.renessans.jvschool.volkov.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IDomainService;
import ru.renessans.jvschool.volkov.tm.command.data.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.dto.Domain;
import ru.renessans.jvschool.volkov.tm.util.DataMarshalizerUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class DataXmlExportCommand extends AbstractDataCommand {

    @NotNull
    private static final String CMD_XML_EXPORT = "data-xml-export";

    @NotNull
    private static final String DESC_XML_EXPORT = "экспортировать домен в xml вид";

    @NotNull
    private static final String NOTIFY_XML_EXPORT = "Происходит процесс выгрузки домена в xml вид...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_XML_EXPORT;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_XML_EXPORT;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.print(NOTIFY_XML_EXPORT);
        @NotNull final Domain domain = new Domain();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.dataExport(domain);
        DataMarshalizerUtil.writeToXml(domain, XML_FILE_LOCATE);

        ViewUtil.print(domain.getUsers());
        ViewUtil.print(domain.getTasks());
        ViewUtil.print(domain.getProjects());
    }

}