package ru.renessans.jvschool.volkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    @Override
    public UserRole[] permissions() {
        return new UserRole[]{UserRole.ADMIN, UserRole.USER};
    }

}